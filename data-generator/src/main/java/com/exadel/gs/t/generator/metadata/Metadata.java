package com.exadel.gs.t.generator.metadata;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * @author Andrey Frunt
 */
@XmlRootElement(name = "metadata")
public class Metadata {
    private List<CategoryMetadata> categoriesMetaData;
    private UserMetadata userMetadata;

    @XmlElement(name = "categoryMetadata")
    public List<CategoryMetadata> getCategoriesMetaData() {
        return categoriesMetaData;
    }

    public void setCategoriesMetaData(List<CategoryMetadata> categoriesMetaData) {
        this.categoriesMetaData = categoriesMetaData;
    }

    public UserMetadata getUserMetadata() {
        return userMetadata;
    }

    public void setUserMetadata(UserMetadata userMetadata) {
        this.userMetadata = userMetadata;
    }
}
