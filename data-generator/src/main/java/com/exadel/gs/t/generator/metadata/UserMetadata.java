package com.exadel.gs.t.generator.metadata;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * @author Andrey Frunt
 */
@XmlRootElement
public class UserMetadata {
    private List<String> firstNames;
    private List<String> lastNames;

    @XmlElement(name = "firstName")
    public List<String> getFirstNames() {
        return firstNames;
    }

    public void setFirstNames(List<String> firstNames) {
        this.firstNames = firstNames;
    }

    @XmlElement(name = "lastName")
    public List<String> getLastNames() {
        return lastNames;
    }

    public void setLastNames(List<String> lastNames) {
        this.lastNames = lastNames;
    }
}
