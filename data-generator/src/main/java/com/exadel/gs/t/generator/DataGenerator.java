package com.exadel.gs.t.generator;

import com.exadel.gs.t.generator.metadata.CategoryMetadata;
import com.exadel.gs.t.generator.metadata.Metadata;
import com.exadel.gs.t.generator.metadata.UserMetadata;
import com.exadel.gs.t.model.Category;
import com.exadel.gs.t.model.Product;
import com.exadel.gs.t.model.ShoppingCart;
import com.exadel.gs.t.model.User;
import com.exadel.gs.t.service.ShoppingCartService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openspaces.core.GigaSpace;
import org.openspaces.remoting.ExecutorProxy;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author Andrey Frunt
 */
@Component
public class DataGenerator {
    private final Log log = LogFactory.getLog(DataGenerator.class);
    private static final String METADATA_XML_FILE = "metadata.xml";
    private static final String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    @Resource
    private GigaSpace gigaSpace;

    @ExecutorProxy
    private ShoppingCartService service;

    private Random random = new Random(System.currentTimeMillis());

    @PostConstruct
    public void generate() throws JAXBException {
        StopWatch task = new StopWatch();
        Metadata metadata = unmarshallMetadata();

        clearOldData();

        List<CategoryMetadata> categoriesMetaData = metadata.getCategoriesMetaData();

        gigaSpace.writeMultiple(generateCategories(categoriesMetaData).toArray());
        log.info(categoriesMetaData.size() + " categories generated and saved");

        long productId = 0;

        for (int categoryIndex = 0; categoryIndex < categoriesMetaData.size(); categoryIndex++) {
            CategoryMetadata categoryMetadata = categoriesMetaData.get(categoryIndex);
            List<Product> products = new ArrayList<Product>(categoryMetadata.getProductCount());
            task.start();
            for (int productIndex = 0; productIndex < categoriesMetaData.get(categoryIndex).getProductCount(); productIndex++) {
                Product product = generateProduct(categoryIndex, categoryMetadata, productId++);
                products.add(product);
            }
            task.stop();
            log.info(products.size() + " products generated in " + task.getLastTaskTimeMillis() + "ms");
            task.start();
            gigaSpace.writeMultiple(products.toArray());
            task.stop();
            log.info(products.size() + " products written to category " + categoryMetadata.getName() + " in " + task.getLastTaskTimeMillis() + "ms");
        }

        List<User> users = generateUsers(metadata.getUserMetadata());
        task.start();
        gigaSpace.writeMultiple(users.toArray());
        task.stop();
        log.info(users.size() + " users saved in " + task.getLastTaskTimeMillis() + "ms");
        service.getShoppingCart(0,true);
        service.getShoppingCart(1,true);
    }

    private List<User> generateUsers(UserMetadata userMetadata) {
        List<String> firstNames = userMetadata.getFirstNames();
        List<String> lastNames = userMetadata.getLastNames();
        List<User> users = new ArrayList<User>();
        long id = 0;
        for (String firstName : firstNames) {
            for (String lastName : lastNames) {
                User user = new User();
                user.setName(firstName + " " + lastName);
                user.setId(id++);
                users.add(user);
            }
        }
        return users;
    }

    private Product generateProduct(long categoryIndex, CategoryMetadata categoryMetadata, long id) {
        Product product = new Product();
        product.setCategoryId(categoryIndex);
        product.setId(id);
        StringBuilder nameBuilder = new StringBuilder();
        List<String> brands = categoryMetadata.getBrands();
        String brand = brands.get(randomInt(brands.size()));
        nameBuilder.append(brand);
        nameBuilder.append(" ").append(categoryMetadata.getProductPrefix());
        nameBuilder.append(" ").append(generateModelLetters()).append(randomInt(10000));
        product.setCount(1000);
        product.setPrice(randomInt(categoryMetadata.getMinPrice().intValue(), categoryMetadata.getMaxPrice().intValue()).doubleValue());
        product.setName(nameBuilder.toString());
        return product;
    }

    private List<Category> generateCategories(List<CategoryMetadata> categoriesMetaData) {
        List<Category> categories = new ArrayList<Category>(categoriesMetaData.size());
        for (int i = 0; i < categoriesMetaData.size(); i++) {
            CategoryMetadata metadata = categoriesMetaData.get(i);
            Category category = new Category();
            category.setName(metadata.getName());
            category.setId((long) i);
            category.setDescription(metadata.getDescription());
            categories.add(category);
        }
        return categories;
    }

    private Metadata unmarshallMetadata() throws JAXBException {
        StopWatch task = new StopWatch();
        task.start();
        JAXBContext ctx = JAXBContext.newInstance(Metadata.class, CategoryMetadata.class);
        Unmarshaller unmarshaller = ctx.createUnmarshaller();
        Metadata metadata = (Metadata) unmarshaller.unmarshal(DataGenerator.class.getClassLoader().getResourceAsStream(METADATA_XML_FILE));
        task.stop();
        log.info("Metadata unmarshalled in " + task.getLastTaskTimeMillis() + "ms");
        return metadata;
    }

    private void clearOldData() {
        StopWatch task = new StopWatch();
        task.start();
        gigaSpace.clear(new Product());
        gigaSpace.clear(new Category());
        gigaSpace.clear(new User());
        gigaSpace.clear(new ShoppingCart());
        task.stop();
        log.info("All previous data removed in " + task.getLastTaskTimeMillis() + "ms");
    }

    private char[] generateModelLetters() {
        Integer letterCount = randomInt(3);
        char[] model = new char[letterCount];
        for (int i = 0; i < letterCount; i++) {
            model[i] = alphabet.charAt(i);
        }
        return model;
    }

    private Integer randomInt(int max) {
        int r = random.nextInt();
        r = r < 0 ? -r : r;
        return r % max;
    }

    private Integer randomInt(int min, int max) {
        int r = randomInt(max);
        r = r < min ? r + min : r;
        r = r > max ? r % max : r;
        return r;
    }
}
