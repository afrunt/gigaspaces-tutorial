package com.exadel.gs.t.service;

import com.exadel.gs.t.model.ShoppingCart;
import org.openspaces.remoting.Routing;

/**
 * @author Andrey Frunt
 */
public interface ShoppingCartService {
    ShoppingCart getShoppingCart(@Routing long userId, boolean create);

    void addToCart(long userId, long productId);

    void addToCart(long userId, long productId, int count);

    void removeFromCart(long userId, long productId);

    void removeFromCart(long userId, long productId, int count);
}
