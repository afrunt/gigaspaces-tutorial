package com.exadel.gs.t.model;

import com.gigaspaces.annotation.pojo.SpaceClass;
import com.gigaspaces.annotation.pojo.SpaceId;
import com.gigaspaces.annotation.pojo.SpaceIndex;
import com.gigaspaces.metadata.index.SpaceIndexType;

import java.io.Serializable;
import java.util.List;

/**
 * @author Andrey Frunt
 */
@SpaceClass
public class ShoppingCart implements Serializable {
    private Long userId;
    private List<CartItem> items;
    private Double total;

    @SpaceId(autoGenerate = false)
    @SpaceIndex(type = SpaceIndexType.BASIC)
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
