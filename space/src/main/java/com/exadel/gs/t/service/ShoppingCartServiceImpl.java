package com.exadel.gs.t.service;

import com.exadel.gs.t.model.Product;
import com.exadel.gs.t.model.ShoppingCart;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openspaces.core.GigaSpace;
import org.openspaces.remoting.RemotingService;

import javax.annotation.Resource;

/**
 * @author Andrey Frunt
 */
@RemotingService
public class ShoppingCartServiceImpl implements ShoppingCartService {
    private final Log log = LogFactory.getLog(ShoppingCartServiceImpl.class);
    @Resource(name = "gigaspace")
    private GigaSpace gigaSpace;
    @Resource(name = "gigaspaceGlobal")
    private GigaSpace gigaSpaceGlobal;
    @Override
    public ShoppingCart getShoppingCart(long userId, boolean create) {
        ShoppingCart shoppingCart = gigaSpace.readById(ShoppingCart.class, userId);
        if(shoppingCart == null && create) {
            shoppingCart = new ShoppingCart();
            shoppingCart.setUserId(userId);
        }
        gigaSpace.write(shoppingCart);
        log.info("Product 0L = " + gigaSpaceGlobal.readById(Product.class,0L));
        log.info("Product 1L = " + gigaSpaceGlobal.readById(Product.class,1L));
        log.info("Product 2L = " + gigaSpaceGlobal.readById(Product.class,2L));
        log.info("Product 3L = " + gigaSpaceGlobal.readById(Product.class,3L));
        log.info("Product 4L = " + gigaSpaceGlobal.readById(Product.class,4L));
        log.info("Product 5L = " + gigaSpaceGlobal.readById(Product.class,5L));
        log.info("Product 100010L = " + gigaSpaceGlobal.readById(Product.class,100010L));
        log.info("Product 100011L = " + gigaSpaceGlobal.readById(Product.class,100011L));
        log.info("Product 100012L = " + gigaSpaceGlobal.readById(Product.class,100012L));
        log.info("Product 100013L = " + gigaSpaceGlobal.readById(Product.class,100013L));
        log.info("Product 100014L = " + gigaSpaceGlobal.readById(Product.class,100014L));
        log.info("Product 100015L = " + gigaSpaceGlobal.readById(Product.class,100015L));
        return shoppingCart;
    }

    @Override
    public void addToCart(long userId, long productId) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void addToCart(long userId, long productId, int count) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void removeFromCart(long userId, long productId) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void removeFromCart(long userId, long productId, int count) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
